package com.klemenzabukovec.simplecalendar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import net.miginfocom.swing.MigLayout;

/*
 * 
 * 	Simple Calendar
 * 
 * 
 */

public class SimpleCalendar {

	private static JFrame frame;
	private static JPanel panel;
	private static JTable calendarTable;
	private static DefaultTableModel tableModel;
	private static JScrollPane scrollPane;
	private static JComboBox monthComboBox;
	private static JButton previousMonthButton, nextMonthButton, todayButton;
	private static JTextField dateTextField, yearTextField;
	private static JLabel dateLabel;

	static int selectedYear, selectedMonth, selectedDay;
	static String[] months = { "January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };

	static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	static HashMap<String, String> dateFileContent = new HashMap<String, String>();
	
	public void drawGui() {

		// read text file containing holiday dates
		DateFileReader dateFileReader = new DateFileReader("prazniki.txt");
		dateFileContent = dateFileReader.readDateFile();
		
		// gui elements
		frame = new JFrame("Calendar");
		new JLabel("Year: ");
		dateLabel = new JLabel("Date: ");
		dateTextField = new JTextField();
		yearTextField = new JTextField();
		previousMonthButton = new JButton("<-");
		nextMonthButton = new JButton("->");
		todayButton = new JButton("TODAY");
		monthComboBox = new JComboBox();

		// set preferred dimensions for text fields
		dateTextField.setPreferredSize(new Dimension(85, 20));
		yearTextField.setPreferredSize(new Dimension(40, 20));

		// set-up layout manager
		MigLayout mig = new MigLayout();
		panel = new JPanel(mig);
		tableModel = new DefaultTableModel() {
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return false;
			}
		};
		

		/*
		 * 
		 * 	create calendar table
		 * 
		 */
		
		calendarTable = new JTable(tableModel);
		scrollPane = new JScrollPane(calendarTable);

		// initial date
		LocalDate currentDate = LocalDate.now();
		selectedDay = currentDate.getDayOfMonth();
		selectedMonth = currentDate.getMonthValue();
		selectedYear = currentDate.getYear();
		selectedDay = currentDate.getDayOfMonth();
		
		// add day column headers
		String[] dayHeaders = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
		for (int i = 0; i < dayHeaders.length; i++) {
			tableModel.addColumn(dayHeaders[i]);
		}

		// disable calendar table resizing
		calendarTable.getTableHeader().setResizingAllowed(false);
		calendarTable.getTableHeader().setReorderingAllowed(false);

		// set calendar table selection rules
		calendarTable.setCellSelectionEnabled(true);
		calendarTable.setColumnSelectionAllowed(false);
		calendarTable.setRowSelectionAllowed(false);
		
		// set calendar table size
		calendarTable.setRowHeight(42);
		tableModel.setColumnCount(7);
		tableModel.setRowCount(6);
		
		// populate combo-box with month names
		for (String month : months) {
			monthComboBox.addItem(month);
		}

		// add gui elements to panel
		frame.add(panel);
		panel.add(dateLabel);
		panel.add(dateTextField, "wrap");
		panel.add(previousMonthButton);
		panel.add(yearTextField, "split");
		panel.add(monthComboBox, "grow, split");
		panel.add(nextMonthButton, "push, al right, wrap");
		panel.add(scrollPane, "span, wrap");
		panel.add(todayButton, "span, center");

		// set-up frame
		frame.setSize(400, 410);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// populate gui element values
		updateCalendar(selectedYear, selectedMonth, selectedDay);

		// calendar table styling
		calendarTable.setDefaultRenderer(calendarTable.getColumnClass(0), new CalendarTableRenderer());
		
		/*
		 * 
		 * action listeners
		 * 
		 */
		
		// previous month button action event
		previousMonthButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedMonth == 1) {
					selectedMonth = 12;
					selectedYear--;
				} else {
					selectedMonth--;
				}
				updateCalendar(selectedYear, selectedMonth, selectedDay);
			}
		});
		// next month button action event
		nextMonthButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedMonth == 12) {
					selectedMonth = 1;
					selectedYear++;
				} else {
					selectedMonth++;
				}
				updateCalendar(selectedYear, selectedMonth, selectedDay);
			}
		});
		// today button action event
		todayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LocalDate currentDate = LocalDate.now();

				selectedDay = currentDate.getDayOfMonth();
				selectedMonth = currentDate.getMonthValue();
				selectedYear = currentDate.getYear();

				updateCalendar(selectedYear, selectedMonth, selectedDay);
			}
		});
		// enter date in text field action event
		dateTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userTypedDate = dateTextField.getText();

				try {
					LocalDate selectedDate = LocalDate.parse(userTypedDate, dateTimeFormatter);

					selectedDay = selectedDate.getDayOfMonth();
					selectedMonth = selectedDate.getMonthValue();
					selectedYear = selectedDate.getYear();
					
					updateCalendar(selectedYear, selectedMonth, selectedDay);

				} catch (DateTimeException dateTimeException) {
					JOptionPane.showMessageDialog(null, "Exception occured: " + dateTimeException + "\n\n"
							+ "Please check that you entered correct date format: dd.mm.yyyy");
				}
			}
		});
		// enter year in text field action event
		yearTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedYear = Integer.parseInt(yearTextField.getText());
				updateCalendar(selectedYear, selectedMonth, selectedDay);
			}
		});
		// select month in combo-box action event
		monthComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedMonth = Month.valueOf((monthComboBox.getSelectedItem().toString()).toUpperCase()).getValue();
				updateCalendar(selectedYear, selectedMonth, selectedDay);
			}
		});
		// click on calendar table cell action event
		calendarTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				int selectedRow = calendarTable.rowAtPoint(e.getPoint());
				int selectedColumn = calendarTable.columnAtPoint(e.getPoint());

				try {
					selectedDay = (Integer) calendarTable.getValueAt(selectedRow, selectedColumn);
					String selectedDateString = LocalDate.of(selectedYear, selectedMonth, selectedDay).format(dateTimeFormatter);
					dateTextField.setText(selectedDateString);
				} catch (NullPointerException nullPointerException) {
					dateTextField.setText("");
				}
			}
		});
		
	}
	
	
	/*
	 * 
	 * updates calendar table data
	 * 
	 */
	private void updateCalendar(int year, int month, int day) {
		int numberOfDaysInMonth, startOfMonthAt;

		// clear calendar table data
		for (int x = 0; x < 6; x++) {
			for (int y = 0; y < 7; y++) {
				tableModel.setValueAt(null, x, y);
			}
		}
		
		// get first day of month and number of days
		LocalDate selectedDate = null;
		try {
			selectedDate = LocalDate.of(year, month, day);
		} catch(DateTimeException dte) {
			// if february
			if (month == 2) {
				if (LocalDate.of(year, month, 1).isLeapYear()) {
					day = 29;
				} else {
					day = 28;
				}
			} else {
				day--;
			}
			
			selectedDate = LocalDate.of(year, month, day);
		}
		
		YearMonth yearMonth = YearMonth.of(year, month);
		
		numberOfDaysInMonth = selectedDate.lengthOfMonth();
		startOfMonthAt = yearMonth.atDay(1).getDayOfWeek().getValue();
				
		numberOfDaysInMonth = selectedDate.lengthOfMonth();
		startOfMonthAt = yearMonth.atDay(1).getDayOfWeek().getValue();
		
		// set month, year, date
		monthComboBox.setSelectedItem(months[month - 1]);
		yearTextField.setText(String.valueOf(year));
		dateTextField.setText(selectedDate.format(dateTimeFormatter));
		
		// populate calendar table cells
		for (int dayNumber = 1; dayNumber <= numberOfDaysInMonth; dayNumber++) {
			int row = (dayNumber + startOfMonthAt - 2) / 7;
			int column = (dayNumber + startOfMonthAt - 2) % 7;
		
			tableModel.setValueAt(dayNumber, row, column);
		}
	}

}
