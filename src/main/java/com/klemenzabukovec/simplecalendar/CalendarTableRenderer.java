package com.klemenzabukovec.simplecalendar;

import java.awt.Color;
import java.awt.Component;
import java.time.LocalDate;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CalendarTableRenderer extends DefaultTableCellRenderer {

	static int counter = 0;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		// different color for sundays
		if (column == 6) {
			setBackground(new Color(255, 220, 51));
		} else {
			setBackground(new Color(255, 255, 255));
		}

		// get cell value
		value = table.getModel().getValueAt(row, column);
		if (value != null) {
			int cellIntegerValue = Integer.parseInt((value).toString());
			
			// loop through HashMap containing holidays
			for (Map.Entry<String, String> entry : SimpleCalendar.dateFileContent.entrySet()) {
				
				// if repeating holiday
				if (entry.getValue().equals("repeat")) {
					LocalDate holidayDate = LocalDate.parse(entry.getKey(), SimpleCalendar.dateTimeFormatter);
					int holidayDay = holidayDate.getDayOfMonth();
					int holidayMonth = holidayDate.getMonthValue();

					if (holidayMonth == SimpleCalendar.selectedMonth) {
						if (holidayDay == cellIntegerValue) {
							setBackground(new Color(153, 204, 255));
						}
					}
				// if not repeating holiday
				} else if (entry.getValue().equals("noRepeat")) {
					LocalDate holidayDate = LocalDate.parse(entry.getKey(), SimpleCalendar.dateTimeFormatter);
					int holidayDay = holidayDate.getDayOfMonth();
					int holidayMonth = holidayDate.getMonthValue();
					int holidayYear = holidayDate.getYear();

					if (holidayYear == SimpleCalendar.selectedYear) {
						if (holidayMonth == SimpleCalendar.selectedMonth) {
							if (holidayDay == cellIntegerValue) {
								setBackground(new Color(153, 204, 255));
							}
						}
					}
				}
			}
		}
		return this;
	}
	
}
