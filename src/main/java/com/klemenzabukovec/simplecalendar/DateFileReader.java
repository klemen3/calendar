package com.klemenzabukovec.simplecalendar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.swing.JOptionPane;

/*
 * 
 *	reads text file containing holiday dates
 *
 */

public class DateFileReader {

	private String fileName;

	// constructor
	public DateFileReader(String fileName) {
		this.fileName = fileName;
	}
	
	// read file and return HashMap of String date values
	public HashMap<String, String> readDateFile() {

		BufferedReader bufferedReader;
		String line = null;
		HashMap<String, String> dateFileContent = new HashMap<String, String>();

		try {
			bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + fileName)));
			while ((line = bufferedReader.readLine()) != null) {
				String[] dateValue = line.split(",");
				dateFileContent.put(dateValue[0], dateValue[1]);
			}
			bufferedReader.close();

		} catch (FileNotFoundException fileNotFoundException) {
			JOptionPane.showMessageDialog(null, "Exception occured: " + fileNotFoundException);
		} catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "Exception occured: " + ioException);
		} catch (NullPointerException nullPointerException) {
			JOptionPane.showMessageDialog(null, "Exception occured: " + nullPointerException + "\n\nFile with holiday dates not found!");
		} catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
			JOptionPane.showMessageDialog(null, "Exception occured: " + arrayIndexOutOfBoundsException);
		}

		return dateFileContent;
	}

}
